# Python 3.7.9
import random
from operator import itemgetter
from collections import Counter, defaultdict

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Exercise-1: Find the 3 largest numbers from a list of 20 random integers
int_list = random.sample(range(1,100), 20)
largest = sorted(int_list, reverse=True)[:3]


# Exercise-2: Add two lists element-wise in a single line of code
first = [1, 2, 3] 
second = [4, 5, 6]
third = [sum(t) for t in zip(first, second)]


# Exercise-3: flatten a list of lists
def get_flatten_list(lol):
    flatten_list = []
    for el in lol:
        if isinstance(el, list):
            flatten_list += el
        else:
            flatten_list.append(el)
    return flatten_list

lol = [[1, 2], 3, [4, 5, 6], [7, 8], 9, 10, [11, 22, 45, 67]]
flatten_list = get_flatten_list(lol)


# Exercise-4: Sort the following list of tuples based on the last item in the tuple in descending order
tuples = [('john', 'A', 15), ('jane', 'B', 12), ('dave', 'B', 10)]
sorted_tuples = sorted(tuples, key = itemgetter(-1))


# Exercise-5: find 
# 1) key(s) that appear in all three dictionaries 
# 2) find keys that appear in any two dictionaries 

first = dict(a=1, b=2, c=3)
second = dict(a=5, d=6, e=7)
third = dict(b=12, f=77)
keys = list(first.keys()) + list(second.keys()) + list(third.keys())
for k, v in Counter(keys).items():
    if v == 3:
        print("key:{} appeared in all dictionaries".format(k))
    elif v == 2:
        print("key:{} appeared in 2 dictionaries".format(k))


# Exercise-6: Show how to define a dictionary such that calling a non-existent key will not raise an error
def no_key():
    print("The key is not defined")

my_dict =defaultdict(no_key)
my_dict.update({
        'a': 1,
        'b': 2 })
print(my_dict['a'])
print(my_dict['c'])


# Exercise-7: Sort dictionary items based on key

# sort the dictionary below based on the date key in chronological order
data =  [{'date': '2020-02-22', 'price': 17.6, 'vol': 120},
         {'date': '2020-01-02', 'price': 32.3, 'vol': 100},
         {'date': '2020-04-13', 'price': -22.4, 'vol': 130}]

sorted_data = sorted(data, key = itemgetter('date'))


# Exercise-8: convert a list of dictionaries to a dictionary of lists
original = [
    {'date': '2020-01-02', 'price': 32.3, 'vol': 100},
    {'date': '2020-02-22', 'price': 17.6, 'vol': 120},
    {'date': '2020-04-13', 'price': -22.4, 'vol': 130}
]

result = {key: [data[key] for data in original] for key in original[0]}


# Exercise-9: plot an 8x8 chessboard with numpy and matplotlib in under 5 lines of code
# Tested in Jupyter Notebook
chess = np.array([[1,0,1,0,1,0,1,0] if i%2==0 else [0,1,0,1,0,1,0,1] for i in range(8)])
plt.figure(figsize=(8,8))
plt.imshow(chess, cmap='gray')
plt.show()


# Exercise-10: using pandas, get the mean of the time series for every half-hour period
# index = pd.date_range('2020-01-01', '2020-04-01', freq='30min', tz='CET', closed=left) # left is not defined

index = pd.date_range('2020-01-01', '2020-04-01', freq='30min', tz='CET')
prices = np.random.normal(25, 10, len(index))
df = pd.DataFrame(data={'prices': prices}, index=index)

df.resample('1h').prices.mean()


# Exercise-11: add a separate column to the above dataframe numbering the half-hours
new_col = [f'HH{i}' for i in range(1,len(index)+1)]
df['half_hour'] = new_col 

        
